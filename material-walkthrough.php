<?php
/*
Plugin Name: Material Design Side Nav
Plugin URI:  http://advisantgroup.com
Description: Add a material design walkthrough to your WordPress site
Version:     0.1.0
Author:      Justin Maurer
Author URI:  http://advisantgroup.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: material_walkthrough
*/

/**
 * set the db version
 */
global $walkthrough_version;
$walkthrough_version = '1.0';

/*
 * Require necessary libraries
 */
define('MATERIAL_WALKTHROUGH_PATH', plugin_dir_path(__FILE__));

if (file_exists(__DIR__ . '/vendor/CMB2/init.php')) {
    require_once __DIR__ . '/vendor/CMB2/init.php';
}
require 'plugin_update_check.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
    'https://kernl.us/api/v1/updates/57042e8a3af9adb81b06ce9d/',
    __FILE__,
    'material-walkthrough',
    1
);

include(MATERIAL_WALKTHROUGH_PATH . 'options-page.php');

/**
 * Activation hook
 */
function materialWalkthroughActivation()
{
    flush_rewrite_rules();
    global $wpdb;
    global $walkthrough_version;

    $charset_collate = $wpdb->get_charset_collate();
    $tipCompletionTableName = $wpdb->prefix . "walkthrough_tip_completion";
    $userCompletionTableName = $wpdb->prefix . "walkthrough_user_completion";
    $tipsTableName = $wpdb->prefix . "walkthrough_tips";

    $tipCompletionSql = "CREATE TABLE `$tipCompletionTableName` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `css_selector` varchar(45) DEFAULT NULL,
      `time_completed` datetime DEFAULT NULL,
      `user_id` int(11) DEFAULT NULL,
      `tip_uuid` varchar(45) DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id_UNIQUE` (`id`)
    ) $charset_collate;";

    $tipsTableSql = "CREATE TABLE `$tipsTableName` (
      `uuid` int(11) NOT NULL,
      `css_selector` varchar(45) DEFAULT NULL,
      `tip_text` varchar(45) DEFAULT NULL,
      PRIMARY KEY (`uuid`),
      UNIQUE KEY `uuid_UNIQUE` (`uuid`)
    ) $charset_collate;";

    $userCompletionSql = "CREATE TABLE `$userCompletionTableName` (
      `user_id` int(11) NOT NULL,
      `tips_completed` varchar(45) DEFAULT NULL,
      PRIMARY KEY (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    ";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($tipCompletionSql);
    dbDelta($tipsTableSql);
    dbDelta($userCompletionSql);

    add_option( "walkthrough_version", $walkthrough_version );
}

register_activation_hook(__FILE__, 'materialWalkthroughActivation');

/**
 * Deactivation hook
 */
function materialWalkthroughDeactivation()
{
    flush_rewrite_rules();
}

register_deactivation_hook(__FILE__, 'materialWalkthroughDeactivation');

/**
 * Load styles, scripts and fonts
 */
function materialWalkthroughNewScripts()
{
    /**
     * Register styles to be used
     */

    wp_register_style('material-design-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', '', null);

    wp_register_style('material-walkthrough-materialize-css',
        plugin_dir_url(__FILE__) . '/vendor/materialize-src/materialize.css',
        array('material-design-icons'), null);

    wp_register_script('materialize-js', plugin_dir_url(__FILE__) . '/vendor/materialize-src/js/bin/materialize.min.js',
        array('jquery'), true);

    wp_register_style('material-walkthrough-styles', plugin_dir_url(__FILE__) . 'material-walkthrough-styles.css',
        array('material-design-icons'), null);

    wp_register_script('material-walkthrough-js', plugin_dir_url(__FILE__) . 'js/material-walkthrough.js',
        array('jquery'), null, true);

    /**
     * Enqueue all dependencies
     */

    wp_enqueue_script('jquery');

    wp_enqueue_script('jquery-ui-core');

    wp_enqueue_script('jquery-ui-effects-core');

    wp_enqueue_style('material-design-icons');

    wp_enqueue_style('material-walkthrough-materialize-css');

    wp_enqueue_script('materialize-js');

    wp_enqueue_style('material-walkthrough-styles');

    wp_enqueue_script('material-walkthrough-js');

}

add_action('wp_enqueue_scripts', 'materialWalkthroughNewScripts');

/**
 * Build options page
 */

materialWalkthroughAdmin();

function materialWalkthroughContent()
{
//    $navItems = materialWalkthroughGetOption('material_walkthrough_walkthrough_item');
}

add_action('wp_footer', 'materialWalkthroughContent');
